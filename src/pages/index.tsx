import React from 'react'

import styles from '../styles/home.module.scss'
import SEO from '../components/SEO'

const Home = () => {
	return (
		<>
			<SEO title="My Portfolio!" excludeTitleSuffix />
			<main className={styles.container}>
				<h1>My Portfolio</h1>
			</main>
		</>
	)
}

export default Home
